/* Generated automatically. */
static const char configuration_arguments[] = "./configure --prefix=/app/out_gcc --with-gmp=/app/out_gcc_prereq/out_gmp --with-mpfr=/app/out_gcc_prereq/out_mpfr/ --with-mpc=/app/out_gcc_prereq/out_mpc --disable-multilib";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
